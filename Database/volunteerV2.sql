-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: volunteer
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `event_details`
--

DROP TABLE IF EXISTS `event_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_details` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(45) DEFAULT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `place` varchar(45) DEFAULT NULL,
  `organized_by` varchar(45) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `contact_num` varchar(45) DEFAULT NULL,
  `contact_email` varchar(45) DEFAULT NULL,
  `total_volunteers` int(11) DEFAULT NULL,
  `needed_volunteers` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `posted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_details`
--

LOCK TABLES `event_details` WRITE;
/*!40000 ALTER TABLE `event_details` DISABLE KEYS */;
INSERT INTO `event_details` VALUES (1,'futura','2018-09-03 21:10:00','2018-07-03 15:50:00','bit','cse',NULL,NULL,NULL,20,20,1,'2018-09-06 19:12:41'),(2,'Abdul Majeed','2018-09-14 19:55:00','2018-09-26 17:50:00','Sathy','BIT',NULL,NULL,NULL,500,500,0,'2018-09-06 19:16:42'),(3,'perps','2018-09-07 09:00:00','2018-09-07 17:00:00','sathy','BIT-CSE',NULL,NULL,NULL,20,20,0,'2018-09-06 22:24:23'),(4,'TimeEvent','2018-09-07 19:00:00','2018-09-07 19:10:00','Emerald Hostel','techEngineerz',NULL,NULL,NULL,4,4,0,'2018-09-07 19:08:07'),(5,'test2','2018-09-07 19:15:00','2018-09-07 19:20:00','em','te',NULL,NULL,NULL,4,4,0,'2018-09-07 19:14:00'),(6,'test3','2018-09-07 19:15:00','2018-09-07 19:25:00','emerald','sdf',NULL,NULL,NULL,4,4,0,'2018-09-07 19:20:16'),(7,'dinner','2018-09-07 19:20:00','2018-09-07 19:25:00','mess','najds',NULL,NULL,NULL,1000,1000,1,'2018-09-07 19:23:23'),(8,'test4','2018-09-20 21:50:00','2018-09-21 15:50:00','earth','organiser','This is Description','98189321','koyya@bit.com',234,234,0,'2018-09-17 21:48:57'),(9,'test5','2018-09-20 21:50:00','2018-09-21 15:50:00','earth','organiser','This is Description','98189321','koyya@bit.com',234,234,1,'2018-09-17 21:53:33'),(10,'eventname','2018-09-17 22:25:00','2018-09-17 22:30:00','India','Org','decsc','98548','asd@gmsil',2,2,1,'2018-09-17 22:27:12');
/*!40000 ALTER TABLE `event_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_mapping`
--

DROP TABLE IF EXISTS `event_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_mapping` (
  `event_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  KEY `event_mapping_fk_idx` (`event_id`),
  KEY `event_mapping_user_login_fk_idx` (`uid`),
  CONSTRAINT `event_mapping_event_details_fk` FOREIGN KEY (`event_id`) REFERENCES `event_details` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `event_mapping_user_login_fk` FOREIGN KEY (`uid`) REFERENCES `user_login` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_mapping`
--

LOCK TABLES `event_mapping` WRITE;
/*!40000 ALTER TABLE `event_mapping` DISABLE KEYS */;
INSERT INTO `event_mapping` VALUES (10,16),(3,14),(8,16),(6,16),(4,16),(6,17),(8,17);
/*!40000 ALTER TABLE `event_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_details` (
  `uid` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `current_addr` varchar(80) DEFAULT NULL,
  `permanent_addr` varchar(80) DEFAULT NULL,
  `phone_no` varchar(15) DEFAULT NULL,
  `alter_phone_no` varchar(15) DEFAULT NULL,
  `blood_group` varchar(5) DEFAULT NULL,
  `skills` varchar(45) DEFAULT NULL,
  `clg_name` varchar(50) DEFAULT NULL,
  `register_status` int(11) DEFAULT '0',
  PRIMARY KEY (`uid`),
  CONSTRAINT `user_details_user_login_fk` FOREIGN KEY (`uid`) REFERENCES `user_login` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES (14,'naveenRAJ2','2018-09-14',NULL,'gsgsf','fdgfd','543543','345345','zasd','fight','iitii',1);
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login`
--

DROP TABLE IF EXISTS `user_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_login` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login`
--

LOCK TABLES `user_login` WRITE;
/*!40000 ALTER TABLE `user_login` DISABLE KEYS */;
INSERT INTO `user_login` VALUES (14,'naveen','naveenraj436@gmail.com','c1a38b8a671f58b20d4079b68d6533216db2a364',1),(15,'navee','dsafa@fdsf','8aefb06c426e07a0a671a1e2488b4858d694a730',0),(16,'abdul','abdul@gmail.com','934385f53d1bd0c1b8493e44d0dfd4c8e88a04bb',0),(17,'NAVEENRAJ V','naveenraj.cs16@bitsathy.ac.in','009ca76b20a14bc629d5e74bbb17acb17c821c55',0);
/*!40000 ALTER TABLE `user_login` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-24 23:29:21
