/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainPackage;

import Connect.DBConnect;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        DBConnect con = new DBConnect();
        String option = request.getParameter("option");
        System.out.println(option);
        try (PrintWriter out = response.getWriter()) {
            try {
                con.getConnection();
                //SIGN UP
                if (option != null && option.equalsIgnoreCase("signup")) {
                    String userName = request.getParameter("user_name");
                    String email = request.getParameter("email");
                    String password = request.getParameter("password");
                    int status = 0;
                    con.insert("INSERT INTO volunteer.user_login VALUES (default,'" + userName + "','" + email + "',SHA1('" + password + "')," + status + ");");
                    response.sendRedirect("index.html");
                }
                //LOGIN
                if (option != null && option.equalsIgnoreCase("login")) {
                    String email = request.getParameter("email");
                    String password = request.getParameter("password");
                    String encryptPwd = PasswordSHA1.encrypt(password);
                    con.read("SELECT * FROM volunteer.user_login WHERE email='" + email + "' and password='" + encryptPwd + "';");
                    if (con.rs.isBeforeFirst()) {
                        HttpSession session = request.getSession(true);
                        while (con.rs.next()) {
                            int status = con.rs.getInt("status");
                            int uid = con.rs.getInt("uid");
                            session.setAttribute("status", status);
                            session.setAttribute("uid", uid);
                        }
                        response.sendRedirect("Events.jsp");
                    } else {
                        out.println("<script>alert(\"Invalid Email or Password\");</script>");
                        response.sendRedirect("home.jsp");

                    }

                }
                
                if (option != null && option.equalsIgnoreCase("gSignIn")) {
                    String imageUrl = request.getParameter("imageUrl");
                    String email = request.getParameter("email");
                    String name = request.getParameter("name");
                    String gid = request.getParameter("id");
                    String encryptPwd = PasswordSHA1.encrypt(gid);
                    System.out.println("Image : "+imageUrl);
                    con.read("SELECT * FROM volunteer.user_login WHERE password='" + encryptPwd+"'");
                    if (con.rs.isBeforeFirst()) {
                        //Google Sign In
                        HttpSession session = request.getSession(true);
                        while (con.rs.next()) {
                            int status = con.rs.getInt("status");
                            int uid = con.rs.getInt("uid");
                            session.setAttribute("status", status);
                            session.setAttribute("uid", uid);
                        }
                        response.sendRedirect("Events.jsp");
                    }
                 else {
                        //Google Sign up
                    int status = 0;
                    HttpSession session = request.getSession(true);
                    
                    con.insert("INSERT INTO volunteer.user_login VALUES (default,'" + name + "','" + email + "','" + encryptPwd + "'," + status + ");");
                    System.out.println("SELECT * FROM volunteer.user_login WHERE password ='" + encryptPwd+"'");
                    con.read("SELECT * FROM volunteer.user_login WHERE password ='" + encryptPwd+"'");
                    if (con.rs.isBeforeFirst()) {       
                        while (con.rs.next()) {
//                            status = con.rs.getInt("status");
                            int uid = con.rs.getInt("uid");
                            session.setAttribute("status", status);
                            session.setAttribute("uid", uid);
                        }
                        response.sendRedirect("Events.jsp");
                    }
                }
                }

            } catch (Exception e) {
                out.print(e);
            } finally {
                try {
                    con.closeConnection();
                } catch (SQLException ex) {
                    Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
