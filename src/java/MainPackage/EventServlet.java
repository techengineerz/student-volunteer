/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainPackage;

import Connect.DBConnect;
import Mail.Java_Mail;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class EventServlet extends HttpServlet {

    /**
     * 1. showNav 
     * 2. showEvents
     * 3. postEvent s
     * 4.displayProfile
     * 5.updateProfile
     * 6.displaynEvents
     * 7.volunteerNow
     * 8.showRegEvent
     * 9.cancelEvent
     * 10.View Registered Events by admin
     * 11.sendMail
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            DBConnect con = new DBConnect();

            try {
                con.getConnection();
                HttpSession session = request.getSession();
                String option = request.getParameter("option");
                String uid = "";
                
                // SHOW NAVBAR
                
                if (option != null && option.equalsIgnoreCase("showNav")) {
                    if (session.getAttribute("uid") != null) {
                        RequestDispatcher rd = request.getRequestDispatcher("navbar.jsp");
                        rd.include(request, response);
                    } else {
                        out.println(" <div style=\"margin-bottom: 20px\">\n"
                                + "            <nav class=\"navbar navbar-light navbar-expand-md navigation-clean-button\">\n"
                                + "                <div class=\"container\"><a class=\"navbar-brand\" href=\"#\">TAMILNADU STUDENT VOLUNTEERS</a><button class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#navcol-1\"><span class=\"sr-only\">Toggle navigation</span><span class=\"navbar-toggler-icon\"></span></button>\n"
                                + "                    <div\n"
                                + "                        class=\"collapse navbar-collapse\" id=\"navcol-1\">\n"
                                + "                        <ul class=\"nav navbar-nav mr-auto\"></ul><span class=\"navbar-text actions\"> <a   href=\"login.jsp\" class=\"login\">Log In</a><a class=\"btn btn-light action-button\" role=\"button\" href=\"SignUp.jsp\">Sign Up</a></span></div>\n"
                                + "                </div>\n"
                                + "            </nav>\n"
                                + "        </div>\n" );
                    }

                }
                
                //SHOW EVENTS
                
                if (option != null && option.equalsIgnoreCase("showEvents")) {

                    String output = "";
                    int adminOrNot = 0;
                    int sessionUid;
                    if (session.getAttribute("uid") != null) {
                        sessionUid = (int) session.getAttribute("uid");
                        adminOrNot = (int) session.getAttribute("status");
                    } else {
                        sessionUid = -1;
                    }

                    if (sessionUid != -1) {
                        con.read("SELECT * FROM volunteer.event_details WHERE status = 0 AND NOT event_details.event_id = ANY (SELECT event_id FROM event_mapping WHERE event_mapping.uid = " + sessionUid + " ) ORDER BY event_id DESC;");
                    } else {
                        con.read("SELECT * FROM volunteer.event_details WHERE status = 0; ");
                    }
                    //con.read("SELECT * FROM volunteer.event_details WHERE status = 0 ORDER BY event_id DESC;");
                    //con.read("SELECT DISTINCT em.uid,ed.* FROM volunteer.event_mapping as em RIGHT JOIN volunteer.event_details as ed ON em.event_id=ed.event_id WHERE ed.status=0 ORDER BY event_id DESC;");//AND (uid = "+userId+" OR uid IS NULL)
                    while (con.rs.next()) {
                        int eventId = con.rs.getInt("event_id");
                        String eventName = con.rs.getString("event_name");
                        String fromDateTime = con.rs.getString("from_date");
                        String toDateTime = con.rs.getString("to_date");
                        String place = con.rs.getString("place");
                        String organizedBy = con.rs.getString("organized_by");
                        int totalVolunteers = con.rs.getInt("total_volunteers");
                        int neededVolunteers = con.rs.getInt("needed_volunteers");
                        String description = con.rs.getString("description");

                        String volunteerNow = "";

                        if (sessionUid != -1 && adminOrNot != 1) {

                            volunteerNow = "<div style=\"text-align: center\"><button id=\"volunteer_now_btn\" class=\"btn btn-outline-danger\" type=\"button\" style=\"width:200px;height:40px;font-size:16px;margin-top:15px;\" onclick=\"volunteerNow(" + eventId + ")\">Volunteer Now</button></div>\n";

                        } else if (adminOrNot == 1) {
                            volunteerNow = "<div style=\"text-align: center\"><form action=\"ViewDetails.jsp\" >"
                                    + "<input type=\"hidden\" name=\"event_id\" value='" + eventId + "'>"
                                    + "<input type=\"submit\"  id=\"volunteer_now_btn\" class=\"btn btn-outline-success\"  style=\"width:200px;height:40px;font-size:16px;margin-top:15px;\" value='View Details'></form></div>\n";
                        } else {

                            volunteerNow = "<div style=\"text-align: center\"><button disabled id=\"volunteer_now_btn\" class=\"btn btn-outline-success\" type=\"button\" style=\"width:200px;height:40px;font-size:16px;margin-top:15px;\">Sign In</button></div>\n";
                        }

                        output += "<div class=\"col-lg-4\">\n"
                                + "                        <div class=\"card mb-5 shadow p-3\">\n"
                                + "                            <div class=\"card-body\" data-target=\"#myModal\" >\n"
                                + "                                <h4 class=\"card-title bolder_text\">" + eventName + "</h4>\n"
                                + "                                <h6 class=\"text-muted card-subtitle mb-2\">" + organizedBy + "</h6>\n"
                                + "                                <p class=\"card-text\" style=\"color:rgb(59,57,57);\">\n"
                                + "                                    <b class=\"bolder_text\">Date : </b>" + fromDateTime + "<br>\n"
                                + "                                    <b class=\"bolder_text\">Location: </b>" + place + "<br>\n"
                                + "                                    <b class=\"bolder_text\">Total Volunteers: </b>" + totalVolunteers + "<br>\n"
                                + "                                </p>\n"
                                + "                             <a href='#' style=\"margin-bottom:5px\" data-toggle=\"modal\" data-target=\"#id_" + eventId + "\">More...</a>"
                                + volunteerNow
                                + "                            </div>\n"
                                + " <div class=\"modal fade\" id=\"id_" + eventId + "\" role=\"dialog\">\n"
                                + "    <div class=\"modal-dialog modal-lg\">\n"
                                + "      <div class=\"modal-content\">\n"
                                + "        <div class=\"modal-body\" style=\"text-align:center\">\n"
                                + "          <p>" + description + ".</p>\n"
                                + "        </div>\n"
                                + "        <div class=\"modal-footer\">\n"
                                + "          <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n"
                                + "        </div>\n"
                                + "      </div>\n"
                                + "    </div>\n"
                                + "  </div>"
                                + "                        </div>\n"
                                + "</div>";

                        //   }
                    }
                    out.println(output);
                }

                // POST EVENTS
                if (option != null && option.equalsIgnoreCase("postEvents")) {
                    String event_name = request.getParameter("event_name");
                    String from_date = request.getParameter("from_date");
                    String to_date = request.getParameter("to_date");
                    String place = request.getParameter("place");
                    String organized_by = request.getParameter("organized_by");
                    String total_volunteers = request.getParameter("total_volunteers");
                    String description = request.getParameter("description");
                    String contact_num = request.getParameter("contact_num");
                    String contact_email = request.getParameter("contact_email");
                    String formatted_from_date, formatted_to_date;
                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
                    DateFormat dateOutputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                    formatted_from_date = dateOutputFormat.format(df.parse(from_date));
                    formatted_to_date = dateOutputFormat.format(df.parse(to_date));
                    System.out.println(formatted_from_date);
                    con.insert("INSERT INTO volunteer.event_details VALUES (default,'" + event_name + "','" + formatted_from_date + "','" + formatted_to_date + "','" + place + "','" + organized_by + "','" + description + "','" + contact_num + "','" + contact_email + "'," + total_volunteers + "," + total_volunteers + ",default,now());");
                    //System.out.println("INSERT INTO volunteer.event_details VALUES (default,'" + event_name + "','" + formatted_from_date + "','" + formatted_to_date + "','" + place + "','" + organized_by + "','"+description+"','"+contact_num+"','"+contact_email+"'," + total_volunteers + "," + total_volunteers + ",default,now());");
                    con.read1("SELECT * FROM volunteer.event_details WHERE event_id= (SELECT MAX(event_id) FROM volunteer.event_details) ;");
                    int eid = 0;
                    if (con.rs1.next()) {
                        eid = con.rs1.getInt("event_id");
                    }
                    con.update("CREATE EVENT IF NOT EXISTS event_" + eid + "\n"
                            + "ON SCHEDULE AT ('" + formatted_to_date + "')\n"
                            + "DO\n"
                            + "	UPDATE volunteer.event_details SET status = 1 WHERE event_id = " + eid + ";");
                    System.out.println("CREATE EVENT IF NOT EXISTS event_" + eid + "\n"
                            + "ON SCHEDULE AT ('" + formatted_to_date + "')\n"
                            + "DO\n"
                            + "	UPDATE volunteer.event_details SET status = 1 WHERE event_id = " + eid + ";");
                }

                //Display Profile
                int regStatus = 0; //Whether register details are filled
                if (option != null && option.equalsIgnoreCase("displayProfile")) {
                    String output = " ";
                    uid = session.getAttribute("uid").toString();
                    con.read("SELECT * FROM volunteer.user_details WHERE uid=" + uid + ";");

                    while (con.rs.next()) {
                        regStatus = con.rs.getInt("register_status");
                    }
                    if (regStatus == 0) {
                        output = "<center>\n"
                                + "                <img src=\"./assets/profile-placeholder.svg\" width=\"100px\" height=\"100px\">\n"
                                + "                </center><div class=\"container\">\n"
                                + "            <h1 class=\"text-center\" style=\"padding-top:20px;\">PROFILE</h1>\n"
                                + "            <form class=\"container\" style=\"padding-bottom:50px;\">\n"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;\">Name</label><input type=\"text\" class=\"form-control align-items-center\" id=\"name\" style=\"width:500px;\" required/></div>\n"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Date of Birth</label><input type=\"date\" class=\"form-control\" id=\"dob\" style=\"width:500px;\" required/></div>\n"
                                + "<div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Gender</label><br><input type=\"radio\" id='gender' name=\"gender\" value=\"Male\"> Male<br>\n"
                                + "                           <input type=\"radio\" id='gender' name=\"gender\" value=\"Female\">Female<br>\n"
                                + "                           <input type=\"radio\" id='gender' name=\"gender\" value=\"Others\">Others<br></div>"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Phone Number</label><input type=\"text\" min='10' max='12' class=\"form-control\" id=\"phone\" style=\"width:500px;\" required /></div>\n"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Alternate Phone Number</label><input type=\"text\" class=\"form-control\" id=\"alt_phone\" style=\"width:500px;\" required/></div>\n"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Current Address</label><textarea class=\"form-control\" id=\"current_addr\"required></textarea></div>\n"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Permanent Address</label><textarea class=\"form-control\" id=\"permanent_addr\"required></textarea></div><input type=\"checkbox\" id=\"fillAddress\" name=\"fillAddress\"/>\n"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Blood Group</label><input type=\"text\" class=\"form-control\" id=\"bg\" style=\"width:500px;\" required/></div>\n"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">College / School Name</label><input type=\"text\" class=\"form-control\" id=\"clg\" style=\"width:500px;\" required /></div>\n"
                                + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Skills</label><input type=\"text\" class=\"form-control\" id=\"skills\" style=\"width:500px;\" required/></div>\n"
                                + "                <button class=\"btn btn-success btn-block\" type=\"button\" onclick=\"updateProfile()\" style=\"width:500px;margin: 0 auto;margin-top: 40px;\">SUBMIT</button>\n"
                                + "            </form>\n"
                                + "        </div>";
                    } else if (1 == regStatus) {
                        con.rs.beforeFirst();
                        while (con.rs.next()) {
                            String name = con.rs.getString("username");
                            String dob = con.rs.getString("dob");
                            String current_addr = con.rs.getString("current_addr");
                            String permanent_addr = con.rs.getString("permanent_addr");
                            String phone = con.rs.getString("phone_no");
                            String alt_phone = con.rs.getString("alter_phone_no");
                            String bg = con.rs.getString("blood_group");
                            String clg = con.rs.getString("clg_name");
                            String skills = con.rs.getString("skills");
                            String profilePic = con.rs.getString("photo");

                            output = "<center>\n"
                                    + "                <img src='" + profilePic + "' width=\"100px\" height=\"100px\">\n"
                                    + "                </center><div class=\"container\">\n"
                                    + "            <h1 class=\"text-center\" style=\"padding-top:20px;\">PROFILE</h1>\n"
                                    + "            <form class=\"container\" style=\"padding-bottom:50px;\">\n"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;\">Name</label><input type=\"text\" value=\"" + name + "\" class=\"form-control align-items-center\" id=\"name\" style=\"width:500px;\" required/></div>\n"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Date of Birth</label><input type=\"date\"  value=\"" + dob + "\" class=\"form-control\" id=\"dob\" style=\"width:500px;\" required/></div>\n"
                                    + "<div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Gender</label><br><input type=\"radio\" id='gender' name=\"gender\" value=\"Male\"> Male<br>\n"
                                    + "                           <input type=\"radio\" id='gender' name=\"gender\" value=\"Female\">Female<br>\n"
                                    + "                           <input type=\"radio\" id='gender' name=\"gender\" value=\"Others\">Others<br></div>"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Phone Number</label><input type=\"text\" min='10' max='12' value=\"" + phone + "\" class=\"form-control\" id=\"phone\" style=\"width:500px;\" required=\"true\"/></div>\n"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Alternate Phone Number</label><input type=\"text\"  min='10' max='12' value=\"" + alt_phone + "\" class=\"form-control\" id=\"alt_phone\" style=\"width:500px;\" required/></div>\n"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Current Address</label><textarea class=\"form-control\" id=\"current_addr\" required>" + current_addr + "</textarea></div>\n"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Permanent Address</label><textarea class=\"form-control\" id=\"permanent_addr\"required>" + permanent_addr + "</textarea></div>\n"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Blood Group</label><input type=\"text\" value=\"" + bg + "\" class=\"form-control\" id=\"bg\" style=\"width:500px;\" required/></div>\n"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">College / School Name</label><input type=\"text\" value=\"" + clg + "\" class=\"form-control\" id=\"clg\" style=\"width:500px;\" required/></div>\n"
                                    + "                <div class=\"form-group\" style=\"width:500px;\"><label style=\"color:rgb(0,0,0);font-size:20px;font-weight:bold;\">Skills</label><input type=\"text\" value=\"" + skills + "\" class=\"form-control\" id=\"skills\" style=\"width:500px;\" required/></div>\n"
                                    + "                <button class=\"btn btn-success btn-block\" type=\"button\" onclick=\"updateProfile()\" style=\"width:500px;margin: 0 auto;margin-top: 40px;\">SUBMIT</button>\n"
                                    + "            </form>\n"
                                    + "        </div>";
                        }
                    }
                    out.println(output);
                }

                //UPDATE PROFILE
                if (option != null && option.equalsIgnoreCase("updateProfile")) {
                    uid = session.getAttribute("uid").toString();
                    con.read("SELECT * FROM volunteer.user_details WHERE uid=" + uid + ";");
                    regStatus = 0;
                    while (con.rs.next()) {
                        regStatus = con.rs.getInt("register_status");
                    }
                    String name = request.getParameter("name");
                    String dob = request.getParameter("dob");
                    String current_addr = request.getParameter("current_addr");
                    String permanent_addr = request.getParameter("permanent_addr");
                    String phone = request.getParameter("phone");
                    String alt_phone = request.getParameter("alt_phone");
                    String bg = request.getParameter("bg");
                    String clg = request.getParameter("clg");
                    String skills = request.getParameter("skills");
                    String gender = request.getParameter("gender");
                    if (regStatus == 0) {
                        System.out.println("INSERT INTO volunteer.user_details VALUES (" + uid + ",'" + name + "','" + dob + "','" + gender + "','" + current_addr + "','" + permanent_addr + "','" + phone + "','" + alt_phone + "','" + bg + "','" + skills + "','" + clg + "',1,default);");
                        con.insert("INSERT INTO volunteer.user_details VALUES (" + uid + ",'" + name + "','" + dob + "','" + gender + "','" + current_addr + "','" + permanent_addr + "','" + phone + "','" + alt_phone + "','" + bg + "','" + skills + "','" + clg + "',1,default);");
                    } else {
                        con.update("UPDATE volunteer.user_details\n"
                                + "SET username='" + name + "', dob='" + dob + "',gender='" + gender + "', current_addr='" + current_addr + "', permanent_addr='" + permanent_addr + "', phone_no='" + phone + "', alter_phone_no='" + alt_phone + "',blood_group='" + bg + "',skills='" + skills + "',clg_name='" + clg + "'\n"
                                + "WHERE uid = " + uid + ";");
                        System.out.println("UPDATE volunteer.user_details\n"
                                + "SET username='" + name + "', dob='" + dob + "',gender='" + gender + "', current_addr='" + current_addr + "', permanent_addr='" + permanent_addr + "', phone_no='" + phone + "', alter_phone_no='" + alt_phone + "',blood_group='" + bg + "',skills='" + skills + "',clg_name='" + clg + "'\n"
                                + "WHERE uid = " + uid + ";");
                    }

                    //System.out.println("INSERT INTO volunteer.user_details VALUES (" + uid + ",'" + name + "','" + dob + "','" + current_addr + "','" + permanent_addr + "','" + phone + "','" + alt_phone + "','" + bg + "','" + qualification + "','" + clg + "',1);");
                    //System.out.println(uid+name+dob+current_addr+permanent_addr+phone+alt_phone+bg+qualification+clg);
                }

                //DISPLAY 3 EVENTS
                if (option != null && option.equalsIgnoreCase("displaynEvents")) {

                }

                //VOLUNTEER NOW
                if (option != null && option.equalsIgnoreCase("volunteerNow")) {
                    if (session != null) {
                        String userid = session.getAttribute("uid").toString();
                        if (userid != null) {
                            //String userid = (String) session.getAttribute("uid");
                            String eventId = request.getParameter("eventId");
                            System.out.println("INSERT INTO volunteer.event_mapping VALUES (" + eventId + "," + userid + ");");
                            con.insert("INSERT INTO volunteer.event_mapping VALUES (" + eventId + "," + userid + ");");

                        } else {
                            System.out.println("Error occured at Volunteer now");
                        }
                    }
                }

                /*
                        Registered Event Page
                 */
                if (option != null && option.equalsIgnoreCase("showRegEvent")) {
                    String output = "";
                    String userid = session.getAttribute("uid").toString();
                    con.read("SELECT em.uid,ed.* FROM volunteer.event_mapping as em INNER JOIN volunteer.event_details as ed ON em.event_id=ed.event_id WHERE uid=" + userid + " AND ed.status=0;");
                    //System.out.println("SELECT em.uid,ed.* FROM volunteer.event_mapping as em INNER JOIN volunteer.event_details as ed ON em.event_id=ed.event_id WHERE uid="+userid+" AND ed.status=0;");
                    while (con.rs.next()) {
                        int eventId = con.rs.getInt("event_id");
                        String eventName = con.rs.getString("event_name");
                        String fromDateTime = con.rs.getString("from_date");
                        String toDateTime = con.rs.getString("to_date");
                        String place = con.rs.getString("place");
                        String organizedBy = con.rs.getString("organized_by");
                        int totalVolunteers = con.rs.getInt("total_volunteers");
                        int neededVolunteers = con.rs.getInt("needed_volunteers");
                        output += "<div class=\"col-lg-4\">\n"
                                + "                        <div class=\"card mb-5 shadow p-3\">\n"
                                + "                            <div class=\"card-body\">\n"
                                + "                                <h4 class=\"card-title bolder_text\">" + eventName + "</h4>\n"
                                + "                                <h6 class=\"text-muted card-subtitle mb-2\">" + organizedBy + "</h6>\n"
                                + "                                <p class=\"card-text\" style=\"color:rgb(59,57,57);\">\n"
                                + "                                    <b class=\"bolder_text\">Date : </b>" + fromDateTime + "<br>\n"
                                + "                                    <b class=\"bolder_text\">Location: </b>" + place + "<br>\n"
                                + "                                    <b class=\"bolder_text\">Total Volunteers: </b>" + totalVolunteers + "<br>\n"
                                + "                                    <!--                            </p><a class=\"card-link\" href=\"#\">Link</a><a class=\"card-link\" href=\"#\">Link</a></div>-->\n"
                                + "                                </p>\n"
                                + "                                <div style=\"text-align: center\"><button id=\"cancel_event_btn\" class=\"btn btn-outline-danger\" type=\"button\" style=\"width:200px;height:40px;font-size:16px;\" onclick=\"cancelEvent(" + eventId + ")\">Cancel</button></div>\n"
                                + "                            </div>\n"
                                + "                        </div>\n"
                                + "</div>";
                    }
                    out.println(output);
                }

                //CANCEL EVENT
                if (option != null && option.equalsIgnoreCase("cancelEvent")) {
                    if (session != null) {
                        String userid = session.getAttribute("uid").toString();
                        System.out.println("CancelEvent");
                        if (userid != null) {
                            //String userid = (String) session.getAttribute("uid");
                            String eventId = request.getParameter("eventId");
                            con.delete("DELETE FROM volunteer.event_mapping WHERE event_id=" + eventId + " AND uid=" + userid + ";");
                            System.out.println("DELETE FROM volunteer.event_mapping WHERE event_id=" + eventId + " AND uid=" + userid + ";");
                            //con.insert("INSERT INTO volunteer.event_mapping VALUES ("+eventId+","+userid+");");
                        } else {
                            out.println("Incorrect username/password");
                            response.sendRedirect("index.html");
                        }
                    }
                }
                //VIEW DETAILS FOR ADMIN
                if (option != null && option.equalsIgnoreCase("viewDetails")) {

                    String eventId = request.getParameter("eventId");
                    String output = "<table> <tr>\n"
                            + "    <th>Username</th>\n"
                            + "    <th>DOB</th>\n"
                            + "    <th>Gender</th>\n"
                            + "    <th>Current Address</th>\n"
                            + "    <th>Permanent Address</th>\n"
                            + "    <th>Phone Number</th>\n"
                            + "    <th>Alternate Phone Number</th>\n"
                            + "    <th>Bloog Group</th>\n"
                            + "    <th>Skills</th>\n"
                            + "    <th>College/School Name</th>\n"
                            + "  </tr>";
                    // response.sendRedirect("ViewDetails.jsp");
                    System.out.println("SELECT username,dob,gender,current_addr,permanent_addr,phone_no,alter_phone_no,blood_group,skills,clg_name FROM volunteer.user_details,volunteer.event_mapping WHERE event_mapping.event_id=" + eventId + ";");
                    con.read("SELECT username,dob,gender,current_addr,permanent_addr,phone_no,alter_phone_no,blood_group,skills,clg_name FROM volunteer.user_details,volunteer.event_mapping WHERE event_mapping.event_id=" + eventId + ";");
                    while (con.rs.next()) {

                        String username = con.rs.getString("username");
                        String dob = con.rs.getString("dob");
                        String gender = con.rs.getString("gender");
                        String current_addr = con.rs.getString("current_addr");
                        String permanent_addr = con.rs.getString("permanent_addr");
                        String phone_no = con.rs.getString("phone_no");
                        String alter_phone_no = con.rs.getString("alter_phone_no");
                        String blood_group = con.rs.getString("blood_group");
                        String skills = con.rs.getString("skills");
                        String clg_name = con.rs.getString("clg_name");

                        output += "  <tr>\n"
                                + "    <td>" + username + "</td>\n"
                                + "    <td>" + dob + "</td>\n"
                                + "    <td>" + gender + "</td>\n"
                                + "    <td>" + current_addr + "</td>\n"
                                + "    <td>" + permanent_addr + "</td>\n"
                                + "    <td>" + phone_no + "</td>\n"
                                + "    <td>" + alter_phone_no + "</td>\n"
                                + "    <td>" + blood_group + "</td>\n"
                                + "    <td>" + skills + "</td>\n"
                                + "    <td>" + clg_name + "</td>\n"
                                + "  </tr>";

                    }
                    output += "</table>";
                    out.println(output);
                }
                //SEND MAIL
                if (option != null && option.equalsIgnoreCase("sendMail")) {
                    String event_id = request.getParameter("eventId");
                    uid = session.getAttribute("uid").toString();
                    con.read("SELECT ul.email,event_name,from_date,to_date,place,contact_num,contact_email FROM volunteer.event_details,volunteer.user_login as ul WHERE event_id=" + event_id + " and uid=" + uid + " ;");
                    while (con.rs.next()) {
                        String toEmail = con.rs.getString("email");
                        String eventName = con.rs.getString("event_name");
                        String fromDateTime = con.rs.getString("from_date");
                        String toDateTime = con.rs.getString("to_date");
                        String place = con.rs.getString("place");
                        Java_Mail.sendmail(toEmail, eventName, fromDateTime, toDateTime, place);
                    }
                }
            } catch (Exception e) {
                out.print(e);
            } finally {
                try {
                    con.closeConnection();
                } catch (SQLException ex) {
                    Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
