/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mail;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Java_Mail {

    public static void sendmail(String toMail, String event_name, String fromDateTime, String toDateTime, String place) {

        try {
            String host = "smtp.gmail.com";
            final String username = "gvms.reply@gmail.com";
            final String password = "adminamnv123";

            String to = toMail;
            String from = "gvms.reply@gmail.com";
            String subject = "Confirmation Mail";
            String messageText = "text email";
            boolean sessionDebug = false;

            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

            Session mailsession = Session.getDefaultInstance(props, null);
            mailsession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailsession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(messageText);
            msg.setContent("<h1><center>Registration Completed Successfully.</center></h1>\n"
                    + "<p> <center>   Event Name : " + event_name + "</center></p>\n"
                    + "<p>     <center> Event From Date :  " + fromDateTime + " </center> </p>\n"
                    + "<p>     <center> Event To Date :  " + toDateTime + " </center> </p>\n"
                     + "<p>     <center> Place :  "+place+" </center> </p>\n"
                    + "<h2><center>Thank you for registering!</center></h2>\n"
                    + "<a href=\"http://localhost:11111/student_volunteers/index.html\">Click here to visit our website!</a>", "text/html");

            Transport transport = mailsession.getTransport("smtp");
            transport.connect(host, username, password);
            transport.sendMessage(msg, msg.getAllRecipients());

            transport.close();
            System.out.println("Message Sent Successfully");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
