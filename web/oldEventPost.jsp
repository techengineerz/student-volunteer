<%-- 
    Document   : EventsPost
    Created on : Sep 24, 2018
    Author     : NaveenRaj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="./js/jquery-3.3.1.min.js"></script>
        <script src="./js/ajax.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
        <!--CSS for header-->
        <link rel="stylesheet" href="./css/events.css">
        <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <style>
            .input-group.date.form_datetime.col-md-5
            {
                width: 300px
            }
        </style>
        
    </head>
    <body>
        <header>
            <div class="container">
                <div id="branding">
                    <h1><span class="highlight">STUDENT</span> VOLUNTEERS </h1>
                </div>
<!--                <nav>
                    <ul>
                        <li><a href="Events.jsp">Home</a></li>
                            <%
                              //  int status = (Integer) session.getAttribute("status");
                              //  if (status == 1)
                                {%>
                        <li class="current"><a href="#">Post Events</a></li>
                            <%}
                            %>
                        <li><a href="#">Profile</a></li>
                        <li><a href="#">Logout</a></li>
                    </ul>
                </nav>-->
            </div>
        </header>
        <div>
            <form>
                <center>
                <img src="./assets/hash_func.png" width="300px" height="300px">
                </center>
                Event Name : <input type="text" id="event_name" name="event_name"  required><br><br>
                From Date :  <div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd/mm/yyyy HH:ii p" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd HH:ii p">
                    <input id="from_date" class="form-control" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div><br><br>
                To Date: <div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd/mm/yyyy HH:ii p" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd HH:ii p">
                    <input id="to_date" class="form-control" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div><br><br>
                <!--From Date : <input type="text" id="from_date" name="from_date"  required><br><br>-->
                <!--To Date : <input type="text" id="to_date" name="to_date"  required><br><br>-->
                Place : <input type="text" id="place" name="place"  required><br><br>
                Organized By : <input type="text" id="organized_by" name="organized_by"  required><br><br>
                Total Volunteers Required : <input type="number" id="total_volunteers" name="total_volunteers"  required><br><br>
                <input type="button" value="SUBMIT"  onclick="postEvents()">
            </form>
        </div>
          <script type="text/javascript">
            $('.form_datetime').datetimepicker({
                //language:  'fr',
                weekStart: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1
            });
        </script>              
    </body>
</html>


<!--
POPUP LOGIN
<div id="modal" class="popupContainer" style="display:none;">
            <header class="popupHeader">
                <span class="header_title">Login</span>
                <span class="modal_close"><i class="fa fa-times"></i></span>
            </header>		
            <section class="popupBody">
                 Username & Password Login form 
                <div class="user_login">
                    <form action="LoginServlet" method="POST">
                        <input type="hidden" name="option" value="login">
                        <label>Email</label>
                        <input type="email" name="email" required/>
                        <br />
                        <label>Password</label>
                        <input type="password" name="password" required/>
                        <br />
                        <div class="action_btns">
                            <div class="one_half last"><a href="LoginServlet" class="btn btn_red">Login</a></div>
                            <div class="one_half last"><input class="btn btn_red" type="submit" value="Login"></div><br>or<br>
                            <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
                        </div>
                    </form>

                    <a href="#" class="forgot_password">Forgot password?</a>
                </div>
            </section>
        </div>-->