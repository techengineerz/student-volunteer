<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Student Volunteer Portal</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Acme">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alef">
        <link rel="stylesheet" href="./css/styles.min.css">
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
        
        <style>
            #volunteer_now_btn:hover{
                    background-color: #dc3545;
            }
            .bolder_text{
                font-weight: bolder;
            }
        </style>
        
    </head>

    <body>
        <div></div>
        <div style="margin-bottom: 20px">
            <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
                <div class="container"><a class="navbar-brand" href="#">TAMILNADU STUDENT VOLUNTEERS</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                    <div
                        class="collapse navbar-collapse" id="navcol-1">
                        <ul class="nav navbar-nav mr-auto"></ul><span class="navbar-text actions"> <a  id="modal_trigger" href="#modal" class="login">Log In</a><a class="btn btn-light action-button" role="button" href="SignUp.jsp">Sign Up</a></span></div>
                </div>
            </nav>
        </div>
        
        <div id="modal" class="popupContainer" style="display:none;">
                <header class="popupHeader">
                    <span class="header_title">Login</span>
                    <span class="modal_close"><i class="fa fa-times"></i></span>
                </header>		
                <section class="popupBody">
                    <!-- Username & Password Login form -->
                    <div class="user_login">
                        <form action="LoginServlet" method="POST">
                            <input type="hidden" name="option" value="login">
                            <label>Email</label>
                            <input type="email" name="email" required/>
                            <br />
                            <label>Password</label>
                            <input type="password" name="password" required/>
                            <br />
                            <div class="action_btns">
                                <!--<div class="one_half last"><a href="LoginServlet" class="btn btn_red">Login</a></div>-->
                                <div class="one_half last"><input class="btn btn_red" type="submit" value="Login"></div>
                            </div>
                        </form>

                        <a href="#" class="forgot_password">Forgot password?</a>
                    </div>
                </section>
            </div>
        
        
        
        <div class="jumbotron jumbotron-fluid jumbotron-main"><script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script> 
            <script src="https://threejs.org/examples/js/libs/stats.min.js"></script>
            <div id="particles-js">
                <canvas class="particles-js-canvas-el" width="1409" height="5000" style="width: 100%; height: 100%;"></canvas>
            </div>
            <div class="container center-vertically-holder" style="margin-top:-20px;">
                <div class="row center-vertically">
                    <div class="col-md-8 offset-sm-0 offset-md-2 text-center" style="margin-top:100px;margin-bottom:100px;">
                        <h1><br></h1>
                        <hr style="border-top:1px;color:rgba(255,255,255,0.9);width:60%;margin:0px;margin-top:-50px;margin-bottom:10px;margin-left:20%;">
                        <p style="font-size:25px;font-family:Alef, sans-serif;"><br><strong>Remember that the happiest people are not those getting more, but those giving more.</strong><br><br></p>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card mb-5 shadow p-3 ">
                            <div class="card-body">
                                <h4 class="card-title bolder_text">FUTURA</h4>
                                <h6 class="text-muted card-subtitle mb-2">National &nbsp;Events</h6>
                                <p class="card-text" style="color:rgb(59,57,57);">
                                    <b class="bolder_text">Date : </b>2018-09-07 <br>
                                    <b class="bolder_text">Location: </b>Bannari Amman Institute<br>
                                    <b class="bolder_text">Organized By: </b>techEngineerz<br>
                                    <!--                            </p><a class="card-link" href="#">Link</a><a class="card-link" href="#">Link</a></div>-->
                                </p>
                                <div style="text-align: center"><button disabled id="volunteer_now_btn" class="btn btn-outline-danger" type="button" style="width:200px;height:40px;font-size:16px;">Volunteer Now</button></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <h4 class="card-title">Title</h4>
                                <h6 class="text-muted card-subtitle mb-2">Subtitle</h6>
                                <p class="card-text">Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p><a class="card-link" href="#">Link</a><a class="card-link"
                                                                                                                                                                                                        href="#">Link</a></div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card mb-4 box-shadow">
                            <div class="card-body">
                                <h4 class="card-title">Title</h4>
                                <h6 class="text-muted card-subtitle mb-2">Subtitle</h6>
                                <p class="card-text">Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p><a class="card-link" href="#">Link</a><a class="card-link"
                                                                                                                                                                                                        href="#">Link</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
          <script type="text/javascript">
              //Pop up when login is clicked
            $("#modal_trigger").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});
            $(".social_login").hide();
            $(".user_login").show();

        </script>
        <div style="text-align: center">
            <button class="btn btn-primary" type="button" style="width:250px;height:50px;background-color:rgb(54,9,234);font-size:20px;" onclick="location.href='Events.jsp' ">View All Events</button></div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
        <script src="./js/script.min.js"></script>
    </body>

</html>