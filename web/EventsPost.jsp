<%-- 
    Document   : Events
    Created on : Sep 24, 2018
    Author     : NaveenRaj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
      response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
                response.setDateHeader("Expires", 0);

                if (session.getAttribute("uid") == null) {
                    response.sendRedirect("index.html");
                }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Events</title>
        <script src="./js/jquery-3.3.1.min.js"></script>
        <script src="./js/ajax.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
        <!--CSS for header-->
        <link rel="stylesheet" href="./css/events.css">
        <link href="./css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <%@include file="NavFiles.jsp" %>
        
        <style>
           
            .input-group.date.form_datetime.col-md-5
            {
                width: 300px
            }
            .form-group{
                margin:0 auto;
                margin-top: 25px;
            }
        </style>
        
    </head>
    <body >
        <%@include file="navbar.jsp" %>

        <div>
            
            <div class="container">
            <h1 class="text-center" style="padding-top:20px;">POST EVENTS</h1>
                <form class="container" style="padding-bottom:50px;">
                    <div class="form-group" style="width:500px;"><label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">Event Name : </label><input type="text" class="form-control align-items-center" style="width:500px;" id="event_name" name="event_name"  required/></div>
                    <div style="width:500px;" class="form-group input-group date form_datetime " data-date="" data-date-format="dd/mm/yyyy HH:ii p" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd HH:ii p">
                   <label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">From Date : </label><input id="from_date" class="form-control" type="text" value="" readonly>
                   <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <div style="width:500px;" class="form-group input-group date form_datetime " data-date="" data-date-format="dd/mm/yyyy HH:ii p" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd HH:ii p">
                   <label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">To Date : </label><input id="to_date" class="form-control" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                    <div class="form-group" style="width:500px;"><label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">Organized By : </label><input type="text" class="form-control align-items-center" style="width:500px;" id="organized_by" name="organized_by"  required/></div>
                    <div class="form-group" style="width:500px;"><label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">Description : </label><textarea class="form-control" id="description" name="description"></textarea></div>
                    <div class="form-group" style="width:500px;"><label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">Contact Number : </label><input type="text" class="form-control align-items-center" style="width:500px;" id="contact_num" name="contact_num" min="10" max="12" required/></div>
                    <div class="form-group" style="width:500px;"><label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">Contact Email : </label><input type="mail" class="form-control align-items-center" style="width:500px;" id="contact_email" name="contact_email"  required/></div>
                    <div class="form-group" style="width:500px;"><label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">Place : </label><textarea class="form-control" id="place" name="place" required=""></textarea></div>
                     <div class="form-group" style="width:500px;"><label style="color:rgb(0,0,0);font-size:20px;font-weight:bold;font-style:normal;">Total Volunteers Required : </label><input type="text" class="form-control align-items-center" style="width:500px;" id="total_volunteers" name="total_volunteers"  required/></div>
                <button class="btn btn-success btn-block" type="button" onclick="postEvents()" style="width:500px;margin: 0 auto;margin-top: 40px;">SUBMIT</button>  
                </form>
            </div>
            
        </div>
          <script type="text/javascript">
            $('.form_datetime').datetimepicker({
                //language:  'fr',
                weekStart: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1
            });
        </script>              
    </body>
</html>
