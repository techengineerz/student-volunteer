<%-- 
    Document   : Events
    Created on : Sep 24, 2018
    Author     : NaveenRaj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Home | Student volunteer Portal</title>
        <script src="./js/jquery-3.3.1.min.js"></script>
        <script src="./js/ajax.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <style>
            body{
                background-image: url(./assets/Abstract-Envelope.svg);
                background-repeat: no-repeat;
                background-size: auto;
                background-size: cover;
            }
            .card {
                box-shadow: 0 1px 2px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
                transition: all 0.3s cubic-bezier(.25,.8,.25,1);
            }

            .card:hover {
                box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);
            }
        </style>
        <%@include file="NavFiles.jsp" %>
    </head>
    <body onload="showNav()">
        <div id="navbar">

        </div>



        <section>
            <div class="container">
                <div class="row" id="events">
                    <!--<div id="events"></div>-->
                </div>    
            </div>
        </section>
    </body>
</html>
