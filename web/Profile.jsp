

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Profile</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
        <%@include file="NavFiles.jsp" %>
        <style>
            .form-group{
                margin:0 auto;
                margin-top: 25px;
            }
        </style>
        <script src="./js/ajax.js"></script>
    </head>

    <body onload="displayProfile()">
        <%@include file="navbar.jsp" %>

        <div id="profileForm" ></div>
        <form action="UploadDownloadFileServlet" method="post" enctype="multipart/form-data">
            Select File to Upload:<input type="file" name="fileName">
            <br>
            <input type="submit" value="Upload">
        </form>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
