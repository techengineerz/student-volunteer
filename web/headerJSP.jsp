<%-- 
    Document   : headerJSP
     Created on : Sep 24, 2018
    Author     : NaveenRaj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header>
     <script src="./js/jquery-3.3.1.min.js"></script>
        <script src="./js/ajax.js"></script>
            <div class="container">
                <div id="branding">
                    <h1><span class="highlight">STUDENT</span> VOLUNTEERS </h1>
                </div>
                <nav>
                    <ul>
                        <li><a href="Events.jsp">Home</a></li>
                            <%
                                int status = (Integer) session.getAttribute("status");
                                if (status == 1)
                                {%>
                        <li class="current"><a href="#">Post Events</a></li>
                            <%}
                            %>
                        <li><a href="#">Profile</a></li>
                        <li><a onClick="logout()" href="home.jsp">Logout</a></li>
                    </ul>
                </nav>
            </div>
        </header>
