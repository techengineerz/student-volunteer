<%-- 
    Document   : login
    Created on : Sep 26, 2018, 1:37:57 AM
    Author     : NaveenRaj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login_volunteer</title>
         <script src="./js/jquery-3.3.1.min.js"></script>
        <script src="./js/ajax.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/login_styles.min.css">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="283689378527-se360s9bi011j64ca25vund2omruf2jr.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
</head>

<body style="height: 100%;background-color: #f1f7fc">
    <div class="login-clean">
        
        <form method="post" action="LoginServlet" style="border-radius:20px;margin-top: 140px;">
              <input type="hidden" name="option" value="login">
            <h2 class="sr-only">Login Form</h2>
            <center> <label style="font-size: 16px"><b>LOGIN</b></label></center>
           
            <div class="illustration"></div>
            <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email"></div>
            <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Password"></div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit">Log In</button></div>
            <div class="action_btns">
                                <!--<div class="one_half last"><a href="LoginServlet" class="btn btn_red">Login</a></div>-->
                           
                                <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" ></div>
                            </div>
            <a href="#" class="forgot">Forgot your email or password?</a></form>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
</body>

</html>
