<div style="margin-bottom: 50px">
            <nav class="navbar navbar-light navbar-expand-md navigation-clean">
                <div class="container"><a class="navbar-brand" href="#">TAMILNADU STUDENT VOLUNTEERS</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                    <div
                        class="collapse navbar-collapse" id="navcol-1">
                        <ul class="nav navbar-nav ml-auto navbar-right">
                            <li class="nav-item" role="presentation"><a class="nav-link" href="Events.jsp">HOME</a></li>
                            
                            <%
                                int status = (Integer) session.getAttribute("status");
                                if (status == 1) {%>
                                <li class="nav-item" role="presentation"><a class="nav-link" href="EventsPost.jsp">POST EVENTS</a></li>   
                                <% }
                                else
                                { %>
                                    <li class="nav-item" role="presentation"><a class="nav-link" href="RegisteredEvents.jsp">REGISTERED EVENTS</a></li>
                            <%}
                            %>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="Profile.jsp">PROFILE</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="LogoutServlet">LOGOUT</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
