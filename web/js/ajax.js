$(document).ready(function () {
    console.log("Ajax ready");
});

function showNav()
{
    var dataString = "option=showNav";
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
            $("#navbar").html(data);
            showEvents();
        }
    });
}

function showEvents()
{
    var dataString = "option=showEvents";
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
            $("#events").html(data);
        }
    });
}
function postEvents()
{
    var dataString = 'event_name=' + $('#event_name').val() + '&from_date=' + $('#from_date').val() + '&to_date=' + $('#to_date').val() + '&place=' + $('#place').val() + '&organized_by=' + $('#organized_by').val() + '&description=' + $('#description').val() +'&contact_num=' + $('#contact_num').val() +'&contact_email=' + $('#contact_email').val() +'&total_volunteers=' + $('#total_volunteers').val() + "&option=postEvents";
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
            alert("Events Uploaded Successfully!");
            //    $("#events").html(data);
        }
    });
}
function logout()
{
    var dataString = "option=logout";
    $.ajax({
        url: "LoginServlet", data: dataString, type: "POST",
        success: function (data)
        {
            // $("#events").html(data);
        }
    });
}

function displayProfile()
{
    var dataString = 'option=displayProfile';
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
            console.log("Profile displayed Successfully!");
            $("#profileForm").html(data);
        }
    });
}
//Updates Profile details in user_details
function updateProfile()
{
    var dataString = 'name=' + $('#name').val() + '&dob=' + $('#dob').val() + "&gender="+$("input[name=gender]:checked").val()+'&phone=' + $('#phone').val() + '&alt_phone=' + $('#alt_phone').val() + '&current_addr=' + $('#current_addr').val() + '&permanent_addr=' + $('#permanent_addr').val() + '&bg=' + $('#bg').val() + '&clg=' + $('#clg').val() + '&skills=' + $('#skills').val() + "&option=updateProfile";
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
            alert("Profile updated Successfully!");
            //    $("#events").html(data);
        }
    });
}
function volunteerNow(eventId)
{
    var dataString = 'option=volunteerNow&eventId='+eventId;
    console.log(dataString);
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
            console.log("Reload showEvents");
            showEvents();
            sendMail(eventId);
        }
    });
}
function showRegEvent()
{
    var dataString = 'option=showRegEvent';
    console.log(dataString);
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
            $("#events").html(data);
        }
    });
}

function cancelEvent(eventId)
{
    var dataString = 'option=cancelEvent&eventId='+eventId;
    console.log(dataString);
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
                showRegEvent();
        }
    });
}

function viewDetails(eventId)
{
    var dataString = 'option=viewDetails&eventId='+eventId;
    console.log(dataString);
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
                 $("#registeredUsers").html(data);
        }
    });
}

function sendMail(eventId)
{
    var dataString = 'option=sendMail&eventId='+eventId;
    console.log(dataString);
    $.ajax({
        url: "EventServlet", data: dataString, type: "POST",
        success: function (data)
        {
          alert("Thank you for volunteering !");
        }
    });
}

function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        var id=profile.getId();
        var name = profile.getName();
        var email = profile.getEmail();
        var imageUrl = profile.getImageUrl();
        var dataString = 'imageUrl='+imageUrl+'&email='+email+'&name='+name+'&id='+id+'&option=gSignIn';
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
//        console.log('Given Name: ' + profile.getGivenName());
//        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
       // var id_token = googleUser.getAuthResponse().id_token;
        //console.log("ID Token: " + id_token);
        $.ajax({
        url: "LoginServlet", data: dataString, type: "POST",
        success: function (data)
        {
//            console.log("Profile displayed Successfully!");
//            $("#profileForm").html(data);
                window.location="Events.jsp";
        }
    });
        
      };


