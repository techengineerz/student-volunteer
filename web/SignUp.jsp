<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign up</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Acme">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alef">
        <link rel="stylesheet" href="./css/styles.min.css">
         <link rel="stylesheet" href="./css/signup_styles.min.css">
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.leanModal.min.js"></script>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
    </head>
    <body>
        <div style="margin-bottom: 20px">
            <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
                <div class="container"><a class="navbar-brand" href="#">TAMILNADU STUDENT VOLUNTEERS</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                    <div
                        class="collapse navbar-collapse" id="navcol-1">
                        <ul class="nav navbar-nav mr-auto"></ul><span class="navbar-text actions"> <a  id="modal_trigger" href="#modal" class="login">Log In</a><a class="btn btn-light action-button" role="button" href="SignUp.jsp">Sign Up</a></span></div>
                </div>
            </nav>
        </div>
        <div class="register-photo">
        <div class="form-container">
            <div class="image-holder"></div>
            <form method="post"action="LoginServlet">
                 <input type="hidden" name="option" value="signUp">
                <h2 class="text-center"><strong>Create</strong> an account.</h2>
                <div class="form-group"><input class="form-control" type="text" id="user_name" name="user_name"   placeholder="Username" required>
                    <input class="form-control" type="email" name="email" id="email" placeholder="Email" style="margin-top:15px;"></div>
                <div class="form-group"><input class="form-control" type="password" name="password"id="password" placeholder="Password"></div>
                <div class="form-group"><input class="form-control" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                  <span id='message'></span></div>
                <div class="form-group">
                    <div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox">I agree to the license terms.</label></div>
                </div>
                <div class="form-group"><button disabled class="btn btn-primary btn-block" id="Button" type="submit">Sign Up</button></div><a href="#" class="already">You already have an account? Login here.</a></form>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>

    </form>
    <script>
        $('#password, #confirm_password').on('keyup', function () {
            if ($('#password').val() == $('#confirm_password').val()) 
            { $('#message').html('Matching').css('color', 'green');
            document.getElementById("Button").disabled = false;} 
            else
            {  $('#message').html('Not Matching').css('color', 'red'); 
        document.getElementById("Button").disabled = true;}
            });
    </script>
</body>
</html>
