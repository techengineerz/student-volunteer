<%-- 
    Document   : ViewDetails
    Created on : Sep 25, 2018, 11:29:45 AM
    Author     : NaveenRaj
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="./js/jquery-3.3.1.min.js"></script>
        <script src="./js/ajax.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">

        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
        <%@include file="NavFiles.jsp" %>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                padding: 20px;
                margin: 20px;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
    </head>
    <%
        String event_id = request.getParameter("event_id");
    %>

    <body onload="viewDetails(<%= event_id%>)">
        <%@include file="navbar.jsp" %>
        <h1 style="text-align:center">Registered Users</h1>

        <div id="registeredUsers"></div>
    </body>
</html>
